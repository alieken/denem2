import os
import csv
import json
import time
import uuid
import logging
import requests 
import sys
from datetime import datetime
from multiprocessing import Pool
from pygeocoder import Geocoder
import reverse_geocoder as rg 
from kafka import KafkaProducer
from urllib.request import urlopen
from CevaData import CevaData
from CassandraHandler import Cassandra
from geopy.geocoders import Nominatim 
import reverse_geocode
from geopy.extra.rate_limiter import RateLimiter
#sys.setrecursionlimit(10000)
geolocator = Nominatim(user_agent="geoapiExercises") 
DEBUG = os.environ['DEBUG']
KAFKA_SERVERS = os.environ['KAFKA_SERVER']
CASSANDRA_SERVERS = os.environ['CASSANDRA_SERVER']
CASSANDRA_PORT = int(os.environ['CASSANDRA_PORT'])

class Processes():
     
    def __init__(self, content):
        self.content = content
        self.csv_file_name = "../data/ceva_data2020.csv" #değiştirildi
        
        self.kafka_topic = "ceva_data"
        logging.debug(sys.getrecursionlimit())
        self.cassandra_keyspace = "ceva_data" #DataBase
        self.cassandra_col_family = "Ceva_Raw_Data3" #Table
        self.cassandra = Cassandra(CASSANDRA_SERVERS, CASSANDRA_PORT, self.cassandra_keyspace, 'prepared_query')

        if DEBUG:
           logging.basicConfig(level=logging.DEBUG)
            #logging.basicConfig(filename='example.log', filemode='w', level=logging.DEBUG)


    def run(self):
        with Pool(3) as p:
            p.map(self.run_parallel, ['csv', 'kafka', 'cassandra'])
            
    def run_parallel(self, backend):
        data_rows = self.read_json(self.content)
        if backend == 'csv':
            #Parse data & write to csv
            self.write_csv(self.csv_file_name, data_rows)
            
        elif backend == 'kafka':
            #Publish data to Kafka
            kafka_producer = self.connect_kafka_producer()
            if kafka_producer:
                self.publish_kafka(kafka_producer, self.kafka_topic, self.content)

        elif backend == 'cassandra':
            #Store data in Cassandra DB
            cassandra_session = self.cassandra.connect_session(self.cassandra_col_family)
            if cassandra_session:
                self.cassandra.store_data(cassandra_session, self.content)
                

    def read_json(self, json_item_param):
        data_rows = []
        for json_item in json_item_param:
            data_rows.append(thread.start_new_thread( solveservice, (json_item) ))
        return data_rows

    def solveservice(self, json_item):
            ceva_data = CevaData()
            ceva_data.gps_in_out_id = json_item['GpsInOutId']
            ceva_data.record_no = json_item['RecordNo']
            ceva_data.device_no = json_item['DeviceNo']
            ceva_data.license_plate = json_item['LicensePlate']
            ceva_data.driver = json_item['Driver']
            ceva_data.date_time_loc = json_item['DateTimeLoc']
            ceva_data.type = json_item['Type']
            ceva_data.speed_km_h = json_item['SpeedKmH']
            ceva_data.distance = json_item['Distance']
            ceva_data.address = json_item['Address']

            plate=json_item['Latitude']
            plate = plate.replace(',', '.')
            #ceva_data.latitude = json_item['Latitude']
            ceva_data.latitude=plate
            json_item['Latitude']=ceva_data.latitude
            plate2=json_item['Longitude']
            plate2 = plate2.replace(',', '.')
            ceva_data.longitude=plate2
            json_item['Longitude']=ceva_data.longitude
            #ceva_data.longitude = json_item['Longitude']
            ceva_data.geographical_region = json_item['GeographicalRegion']
            ceva_data.pause_duration = json_item['PauseDuration']
            ceva_data.idling_duration = json_item['IdlingDuration']
            ceva_data.ignition_duration = json_item['IgnitionDuration']
            ceva_data.user_id = json_item['UserId']
            ceva_data.insert_date = json_item['InsertDate']
            ceva_data.upd_user_id = json_item['UpdUserId']
            ceva_data.update_date = json_item['UpdateDate']
            serviceid=json_item['ServiceId']            
            ceva_data.service_id = serviceid
            address = self.getplace(plate, plate2)
            ceva_data.Detay=address
            city = address.get('province', '')
            ceva_data.City=city
            if serviceid == 2 or serviceid==1:
               plate=self.setplate(ceva_data.license_plate, serviceid)
               ceva_data.license_plate=plate
               json_item['LicensePlate']=ceva_data.license_plate
            elif serviceid==5 or serviceid==3:
               plate=self.setplate(ceva_data.license_plate, serviceid)
               ceva_data.license_plate=plate 
               json_item['LicensePlate']=ceva_data.license_plate
            ceva_data.status = json_item['Status']
            ceva_data.EngineStatus= json_item['EngineStatus']
            ceva_data.IgnittonStatus= json_item['IgnittonStatus']
            ceva_data.IdleSpeedStatus= json_item['IdleSpeedStatus']
            ceva_data.OverSpeedStatus= json_item['OverSpeedStatus']
            ceva_data.HoodStatus= json_item['HoodStatus']
            ceva_data.DoorStatus= json_item['DoorStatus']
            ceva_data.GpsCoonStatus= json_item['GpsCoonStatus']
            ceva_data.FuelUsed= json_item['FuelUsed']
            ceva_data.FuelLevel= json_item['FuelLevel']
            ceva_data.EngineFuelRate= json_item['EngineFuelRate']
            ceva_data.VehicleType= json_item['VehicleType']
            json_item['City']=ceva_data.City
            json_item['Detay']=str(ceva_data.Detay)
            return ceva_data

    def getplace(self, lat, lon):
       geolocator = Nominatim(user_agent="AIzaSyBxZvwK89pBhT2DA8Woo2FYfeuNnJi4KFc") 
       Latitude = lat
       Longitude = lon
       reverse = RateLimiter(geolocator.reverse, min_delay_seconds=1)
       location = reverse((lat, lon), language='tr', exactly_one=True)
       address = location.raw['address'] 
       return address
       
    def getcity(self, addr, serviceid):
       if serviceid==5:
          split1= addr.split(",")
          split2= split1[2].split("/")
          city= split2[1]
       return city
       
    def setplate(self, plate, serviceid):
       if serviceid==5 or serviceid==3:
          newplate=plate.replace(' ', '-')
       elif serviceid==2 or serviceid==1:
          newplate=""
          tutucuilktire=1;
          tutucusectire=0;
          l1=[c for c in plate]   # in Python, a string is just a sequence, so we can iterate over it!
          l2=[ord(c) for c in plate]
          for i in range(0, len(l2)):
             if l2[i]>47 and l2[i]<58:
                if tutucusectire==0:
                   newplate=newplate+l1[i]
                else :
                   newplate=newplate+"-"+l1[i] 
                   tutucusectire=0
             else:
                if tutucuilktire==1:
                   newplate=newplate+"-"+l1[i]
                   tutucuilktire=0
                   tutucusectire=1
                else:
                   newplate=newplate+l1[i]
       return newplate
         
    def write_csv(self, file_name, data_to_write):
        with open(file_name, mode='a') as ceva_data:
            data_writer = csv.writer(ceva_data, delimiter=',', lineterminator='\r\n', quotechar="\"")

            #Only create and write header if it does not exist
            if ceva_data.tell() == 0:
                headers = list(vars(data_to_write[0]).keys())
                for x in range(0, len(headers)):
                    headers[x] = headers[x][1:].upper()
                data_writer.writerow(headers)

            for row in data_to_write:
                data_writer.writerow(list(vars(row).values()))

            ceva_data.close()

    def connect_kafka_producer(self):
        _producer = None
        try:
            _producer = KafkaProducer(bootstrap_servers=KAFKA_SERVERS)
        except Exception as ex:
            logging.debug('Exception while connecting Kafka')
            logging.debug(str(ex))
        else:
            logging.debug('Kafka connected successfully.')
        finally:
            return _producer

    def publish_kafka(self, producer_instance, topic_name, data_to_stream):
        for rec in data_to_stream:
            try:
                key_bytes = bytes(str(rec['LicensePlate']), encoding='utf-8')
                value_bytes = bytes(json.dumps(rec), encoding='utf-8')
                producer_instance.send(topic_name, key=key_bytes, value=value_bytes)
            except Exception as ex:
                logging.debug('Exception while publishing message')
                logging.debug(str(ex))

        try:
            # key_bytes = bytes(str(rec['LicensePlate']), encoding='utf-8')
            # value_bytes = bytes(json.dumps(rec), encoding='utf-8')
            # producer_instance.send(topic_name, key=key_bytes, value=value_bytes)
            producer_instance.flush()
        except Exception as ex:
            logging.debug('Exception while flushing published messages')
            logging.debug(str(ex))
        else:
            logging.debug('Message published successfully.')

# Uncomment if necessary
'''
def is_dublicate(file_name, compare_value):
    with open(file_name) as ceva_data:
        data_reader = csv.reader(ceva_data, delimiter=',')
        line_count = 0
        for row in data_reader:
            if not line_count == 0 and row[0] == str(compare_value):
                return True

            line_count += 1

        ceva_data.close()

    return False
'''

